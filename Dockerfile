FROM openjdk:11.0.1-jre-slim-stretch


ADD /target/nsg-poc-RELEASE.jar nsg-poc-RELEASE.jar

EXPOSE 80
CMD [ \
  "-XX:+UseContainerSupport", \
  "-XX:+UnlockExperimentalVMOptions", \
  "-Djava.security.egd=file:/dev/./urandom", \
  "-Dspring.profiles.active=default", \
  "-Dhttps.protocols=TLSv1.2", \
  "-Djdk.tls.server.protocols=TLSv1.2", \
  "-Djdk.tls.client.protocols=TLSv1.2", \
  "-jar", \
  "/nsg-poc-RELEASE.jar" \
  ]
ENTRYPOINT ["java"]
