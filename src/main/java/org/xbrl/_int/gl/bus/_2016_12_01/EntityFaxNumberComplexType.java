//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.03.06 at 12:22:27 AM EET 
//


package org.xbrl._int.gl.bus._2016_12_01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.xbrl._int.gl.gen._2016_12_01.FaxNumberItemType;
import org.xbrl._int.gl.gen._2016_12_01.FaxNumberUsageItemType;


/**
 * <p>Java class for entityFaxNumberComplexType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entityFaxNumberComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/bus/2016-12-01}entityFaxNumberUsage" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/bus/2016-12-01}entityFaxNumber" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entityFaxNumberComplexType", propOrder = {
    "entityFaxNumberUsage",
    "entityFaxNumber"
})
public class EntityFaxNumberComplexType {

    @XmlElement(nillable = true)
    protected FaxNumberUsageItemType entityFaxNumberUsage;
    @XmlElement(nillable = true)
    protected FaxNumberItemType entityFaxNumber;
    @XmlAttribute(name = "id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the entityFaxNumberUsage property.
     * 
     * @return
     *     possible object is
     *     {@link FaxNumberUsageItemType }
     *     
     */
    public FaxNumberUsageItemType getEntityFaxNumberUsage() {
        return entityFaxNumberUsage;
    }

    /**
     * Sets the value of the entityFaxNumberUsage property.
     * 
     * @param value
     *     allowed object is
     *     {@link FaxNumberUsageItemType }
     *     
     */
    public void setEntityFaxNumberUsage(FaxNumberUsageItemType value) {
        this.entityFaxNumberUsage = value;
    }

    /**
     * Gets the value of the entityFaxNumber property.
     * 
     * @return
     *     possible object is
     *     {@link FaxNumberItemType }
     *     
     */
    public FaxNumberItemType getEntityFaxNumber() {
        return entityFaxNumber;
    }

    /**
     * Sets the value of the entityFaxNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link FaxNumberItemType }
     *     
     */
    public void setEntityFaxNumber(FaxNumberItemType value) {
        this.entityFaxNumber = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
