//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.03.06 at 12:22:27 AM EET 
//


package org.xbrl._int.gl.cor._2016_12_01;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for identifierExternalReferenceComplexType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="identifierExternalReferenceComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/cor/2016-12-01}identifierAuthorityCode" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/cor/2016-12-01}identifierAuthority" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/cor/2016-12-01}identifierAuthorityVerificationDate" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "identifierExternalReferenceComplexType", propOrder = {
    "identifierAuthorityCode",
    "identifierAuthority",
    "identifierAuthorityVerificationDate"
})
public class IdentifierExternalReferenceComplexType {

    @XmlElement(nillable = true)
    protected IdentifierAuthorityCodeItemType identifierAuthorityCode;
    @XmlElement(nillable = true)
    protected IdentifierAuthorityItemType identifierAuthority;
    @XmlElement(nillable = true)
    protected IdentifierAuthorityVerificationDateItemType identifierAuthorityVerificationDate;
    @XmlAttribute(name = "id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the identifierAuthorityCode property.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierAuthorityCodeItemType }
     *     
     */
    public IdentifierAuthorityCodeItemType getIdentifierAuthorityCode() {
        return identifierAuthorityCode;
    }

    /**
     * Sets the value of the identifierAuthorityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierAuthorityCodeItemType }
     *     
     */
    public void setIdentifierAuthorityCode(IdentifierAuthorityCodeItemType value) {
        this.identifierAuthorityCode = value;
    }

    /**
     * Gets the value of the identifierAuthority property.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierAuthorityItemType }
     *     
     */
    public IdentifierAuthorityItemType getIdentifierAuthority() {
        return identifierAuthority;
    }

    /**
     * Sets the value of the identifierAuthority property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierAuthorityItemType }
     *     
     */
    public void setIdentifierAuthority(IdentifierAuthorityItemType value) {
        this.identifierAuthority = value;
    }

    /**
     * Gets the value of the identifierAuthorityVerificationDate property.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierAuthorityVerificationDateItemType }
     *     
     */
    public IdentifierAuthorityVerificationDateItemType getIdentifierAuthorityVerificationDate() {
        return identifierAuthorityVerificationDate;
    }

    /**
     * Sets the value of the identifierAuthorityVerificationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierAuthorityVerificationDateItemType }
     *     
     */
    public void setIdentifierAuthorityVerificationDate(IdentifierAuthorityVerificationDateItemType value) {
        this.identifierAuthorityVerificationDate = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
