//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-147 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.03.06 at 12:22:27 AM EET 
//


package org.xbrl._int.gl.cor._2016_12_01;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.xbrl._int.gl.bus._2016_12_01.IdentifierAddressComplexType;
import org.xbrl._int.gl.bus._2016_12_01.IdentifierPurposeItemType;
import org.xbrl._int.gl.gen._2016_12_01.ActiveItemType;
import org.xbrl._int.gl.gen._2016_12_01.IdentifierOrganizationTypeItemType;
import org.xbrl._int.gl.gen._2016_12_01.IdentifierTypeItemType;


/**
 * <p>Java class for identifierReferenceComplexType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="identifierReferenceComplexType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/cor/2016-12-01}identifierCode" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/cor/2016-12-01}identifierExternalReference" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/cor/2016-12-01}identifierOrganizationType" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/cor/2016-12-01}identifierOrganizationTypeDescription" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/cor/2016-12-01}identifierDescription" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/cor/2016-12-01}identifierType" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/cor/2016-12-01}identifierCategory" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/cor/2016-12-01}identifierEMail" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/cor/2016-12-01}identifierPhoneNumber" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/cor/2016-12-01}identifierFaxNumber" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/bus/2016-12-01}identifierPurpose" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/bus/2016-12-01}identifierAddress" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/cor/2016-12-01}identifierContactInformationStructure" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.xbrl.org/int/gl/cor/2016-12-01}identifierActive" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "identifierReferenceComplexType", propOrder = {
    "identifierCode",
    "identifierExternalReference",
    "identifierOrganizationType",
    "identifierOrganizationTypeDescription",
    "identifierDescription",
    "identifierType",
    "identifierCategory",
    "identifierEMail",
    "identifierPhoneNumber",
    "identifierFaxNumber",
    "identifierPurpose",
    "identifierAddress",
    "identifierContactInformationStructure",
    "identifierActive"
})
public class IdentifierReferenceComplexType {

    @XmlElement(nillable = true)
    protected IdentifierCodeItemType identifierCode;
    @XmlElement(nillable = true)
    protected List<IdentifierExternalReferenceComplexType> identifierExternalReference;
    @XmlElement(nillable = true)
    protected IdentifierOrganizationTypeItemType identifierOrganizationType;
    @XmlElement(nillable = true)
    protected IdentifierOrganizationTypeDescriptionItemType identifierOrganizationTypeDescription;
    @XmlElement(nillable = true)
    protected IdentifierDescriptionItemType identifierDescription;
    @XmlElement(nillable = true)
    protected IdentifierTypeItemType identifierType;
    @XmlElement(nillable = true)
    protected IdentifierCategoryItemType identifierCategory;
    @XmlElement(nillable = true)
    protected List<IdentifierEmailAddressStructureComplexType> identifierEMail;
    @XmlElement(nillable = true)
    protected List<IdentifierPhoneNumberComplexType> identifierPhoneNumber;
    @XmlElement(nillable = true)
    protected List<IdentifierFaxNumberComplexType> identifierFaxNumber;
    @XmlElement(namespace = "http://www.xbrl.org/int/gl/bus/2016-12-01", nillable = true)
    protected IdentifierPurposeItemType identifierPurpose;
    @XmlElement(namespace = "http://www.xbrl.org/int/gl/bus/2016-12-01", nillable = true)
    protected List<IdentifierAddressComplexType> identifierAddress;
    @XmlElement(nillable = true)
    protected List<IdentifierContactInformationStructureTupleType> identifierContactInformationStructure;
    @XmlElement(nillable = true)
    protected ActiveItemType identifierActive;
    @XmlAttribute(name = "id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Gets the value of the identifierCode property.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierCodeItemType }
     *     
     */
    public IdentifierCodeItemType getIdentifierCode() {
        return identifierCode;
    }

    /**
     * Sets the value of the identifierCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierCodeItemType }
     *     
     */
    public void setIdentifierCode(IdentifierCodeItemType value) {
        this.identifierCode = value;
    }

    /**
     * Gets the value of the identifierExternalReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the identifierExternalReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdentifierExternalReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IdentifierExternalReferenceComplexType }
     * 
     * 
     */
    public List<IdentifierExternalReferenceComplexType> getIdentifierExternalReference() {
        if (identifierExternalReference == null) {
            identifierExternalReference = new ArrayList<IdentifierExternalReferenceComplexType>();
        }
        return this.identifierExternalReference;
    }

    /**
     * Gets the value of the identifierOrganizationType property.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierOrganizationTypeItemType }
     *     
     */
    public IdentifierOrganizationTypeItemType getIdentifierOrganizationType() {
        return identifierOrganizationType;
    }

    /**
     * Sets the value of the identifierOrganizationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierOrganizationTypeItemType }
     *     
     */
    public void setIdentifierOrganizationType(IdentifierOrganizationTypeItemType value) {
        this.identifierOrganizationType = value;
    }

    /**
     * Gets the value of the identifierOrganizationTypeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierOrganizationTypeDescriptionItemType }
     *     
     */
    public IdentifierOrganizationTypeDescriptionItemType getIdentifierOrganizationTypeDescription() {
        return identifierOrganizationTypeDescription;
    }

    /**
     * Sets the value of the identifierOrganizationTypeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierOrganizationTypeDescriptionItemType }
     *     
     */
    public void setIdentifierOrganizationTypeDescription(IdentifierOrganizationTypeDescriptionItemType value) {
        this.identifierOrganizationTypeDescription = value;
    }

    /**
     * Gets the value of the identifierDescription property.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierDescriptionItemType }
     *     
     */
    public IdentifierDescriptionItemType getIdentifierDescription() {
        return identifierDescription;
    }

    /**
     * Sets the value of the identifierDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierDescriptionItemType }
     *     
     */
    public void setIdentifierDescription(IdentifierDescriptionItemType value) {
        this.identifierDescription = value;
    }

    /**
     * Gets the value of the identifierType property.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierTypeItemType }
     *     
     */
    public IdentifierTypeItemType getIdentifierType() {
        return identifierType;
    }

    /**
     * Sets the value of the identifierType property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierTypeItemType }
     *     
     */
    public void setIdentifierType(IdentifierTypeItemType value) {
        this.identifierType = value;
    }

    /**
     * Gets the value of the identifierCategory property.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierCategoryItemType }
     *     
     */
    public IdentifierCategoryItemType getIdentifierCategory() {
        return identifierCategory;
    }

    /**
     * Sets the value of the identifierCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierCategoryItemType }
     *     
     */
    public void setIdentifierCategory(IdentifierCategoryItemType value) {
        this.identifierCategory = value;
    }

    /**
     * Gets the value of the identifierEMail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the identifierEMail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdentifierEMail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IdentifierEmailAddressStructureComplexType }
     * 
     * 
     */
    public List<IdentifierEmailAddressStructureComplexType> getIdentifierEMail() {
        if (identifierEMail == null) {
            identifierEMail = new ArrayList<IdentifierEmailAddressStructureComplexType>();
        }
        return this.identifierEMail;
    }

    /**
     * Gets the value of the identifierPhoneNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the identifierPhoneNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdentifierPhoneNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IdentifierPhoneNumberComplexType }
     * 
     * 
     */
    public List<IdentifierPhoneNumberComplexType> getIdentifierPhoneNumber() {
        if (identifierPhoneNumber == null) {
            identifierPhoneNumber = new ArrayList<IdentifierPhoneNumberComplexType>();
        }
        return this.identifierPhoneNumber;
    }

    /**
     * Gets the value of the identifierFaxNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the identifierFaxNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdentifierFaxNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IdentifierFaxNumberComplexType }
     * 
     * 
     */
    public List<IdentifierFaxNumberComplexType> getIdentifierFaxNumber() {
        if (identifierFaxNumber == null) {
            identifierFaxNumber = new ArrayList<IdentifierFaxNumberComplexType>();
        }
        return this.identifierFaxNumber;
    }

    /**
     * Gets the value of the identifierPurpose property.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierPurposeItemType }
     *     
     */
    public IdentifierPurposeItemType getIdentifierPurpose() {
        return identifierPurpose;
    }

    /**
     * Sets the value of the identifierPurpose property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierPurposeItemType }
     *     
     */
    public void setIdentifierPurpose(IdentifierPurposeItemType value) {
        this.identifierPurpose = value;
    }

    /**
     * Gets the value of the identifierAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the identifierAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdentifierAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IdentifierAddressComplexType }
     * 
     * 
     */
    public List<IdentifierAddressComplexType> getIdentifierAddress() {
        if (identifierAddress == null) {
            identifierAddress = new ArrayList<IdentifierAddressComplexType>();
        }
        return this.identifierAddress;
    }

    /**
     * Gets the value of the identifierContactInformationStructure property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the identifierContactInformationStructure property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdentifierContactInformationStructure().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IdentifierContactInformationStructureTupleType }
     * 
     * 
     */
    public List<IdentifierContactInformationStructureTupleType> getIdentifierContactInformationStructure() {
        if (identifierContactInformationStructure == null) {
            identifierContactInformationStructure = new ArrayList<IdentifierContactInformationStructureTupleType>();
        }
        return this.identifierContactInformationStructure;
    }

    /**
     * Gets the value of the identifierActive property.
     * 
     * @return
     *     possible object is
     *     {@link ActiveItemType }
     *     
     */
    public ActiveItemType getIdentifierActive() {
        return identifierActive;
    }

    /**
     * Sets the value of the identifierActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActiveItemType }
     *     
     */
    public void setIdentifierActive(ActiveItemType value) {
        this.identifierActive = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
