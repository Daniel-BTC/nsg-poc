package com.balances.nsgpoc.controller;

import com.balances.nsgpoc.utils.ErrorResponse;
import com.balances.nsgpoc.utils.NSGGlobalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@ControllerAdvice
public class ExceptionHandlingController {

  private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlingController.class);

  @ApiIgnore
  @ExceptionHandler(NSGGlobalException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public ErrorResponse handleSecurityException(NSGGlobalException se) {
    return new ErrorResponse(se.getMessage());
  }
}
