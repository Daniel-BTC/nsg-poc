package com.balances.nsgpoc.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.apache.commons.text.StringEscapeUtils.escapeHtml4;

@RestController
@RequestMapping("/api/v1/ping")
public class PingController {

    /**
     * Return 'ping' String.
     * @return 'ping' message.
     */
    @GetMapping(produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> getPing() {
        return new ResponseEntity<>("ping", HttpStatus.OK);
    }

    /**
     * Return back request parameter.
     * @param param request parameter
     * @return request parameter
     */
    @GetMapping(value = "/{param}", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> getPingWithParameter(@PathVariable("param") String param) {
        return new ResponseEntity<>(escapeHtml4(param),HttpStatus.OK);
    }
}