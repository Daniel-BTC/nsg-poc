package com.balances.nsgpoc.config;

import com.balances.nsgpoc.manager.ReportManager;
import com.balances.nsgpoc.model.gl.XbrliXbrl;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.xbrl._2003.instance.Xbrl;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.*;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;

@Configuration
public class ApplicationConfig {

  private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ReportManager.class);

  private static final String XSD_VALIDATION_PATH = "xsd/gl/plt/case-c-b-m/xbrl-instance-2003-12-31.xsd";
  private static final Class<?>[] FILING_TYPES = new Class[] {
          Xbrl.class,
          org.xbrl._2003.linkbase.Linkbase.class,
          org.xbrl._2003.instance.ObjectFactory.class,
          org.xbrl._2003.linkbase.FootnoteLink.class,
          org.xbrl._int.gl.cor._2016_12_01.AccountingEntriesComplexType.class
  };
  /**
   * Create marshaller for XbrliXbrl class.
   *
   * @return marshaller marshaller
   * @throws JAXBException
   */
  @Bean
  public Marshaller getXbrliMarshaller() throws JAXBException, SAXException {

    JAXBContext jaxbContext = JAXBContext.newInstance(FILING_TYPES);
    Marshaller marshaller = jaxbContext.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

    // temporary xml validation
     // addEventHandlerToMarshaller(marshaller);

    return marshaller;
  }

  private void addEventHandlerToMarshaller(Marshaller marshaller)
      throws SAXException, JAXBException {
    SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
    try {
      File resource =
          new ClassPathResource(XSD_VALIDATION_PATH).getFile();
      Schema schema = schemaFactory.newSchema(resource);
      marshaller.setSchema(schema);
      marshaller.setEventHandler(reportErrorHandler);
    } catch (Exception ioe) {
      LOGGER.error(ioe.getMessage());
    }
  }

  private static ValidationEventHandler reportErrorHandler =
      new ValidationEventHandler() {
        @Override
        public boolean handleEvent(ValidationEvent event) {
          LOGGER.error(event.getMessage());
          LOGGER.error(
              String.format(
                  "ERROR: %s (%d, %d) Severity: %s",
                  event.getMessage(),
                  event.getLocator().getLineNumber(),
                  event.getLocator().getColumnNumber(),
                  event.getSeverity()));
          return false;
        }
      };
}
