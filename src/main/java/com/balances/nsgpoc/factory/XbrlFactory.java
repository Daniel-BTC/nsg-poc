package com.balances.nsgpoc.factory;

import com.balances.nsgpoc.model.Company;
import com.balances.nsgpoc.model.gl.ObjectFactory;
import com.balances.nsgpoc.model.gl.XbrliXbrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Optional;

@Component
public class XbrlFactory {

  private static String CONTEXT_REF = "now";
  private static String SOURCE_APP = "NSG PoC";
  private static String DEFAULT_CURRENCY = "iso4217:nok";
  private static String DEFAULT_LANGUAGE = "iso639:nb";
  private static String JOURNAL = "journal";
  private static String CREATOR = "Accountflow";

  @Autowired private ObjectFactory objectFactory;

  public void setObjectFactory(ObjectFactory objectFactory) {
    this.objectFactory = objectFactory;
  }

  public XbrliXbrl.GlCorAccountingEntries.GlCorDocumentInfo createGlCorDocumentInfo() {

    var gl = new XbrliXbrl.GlCorAccountingEntries.GlCorDocumentInfo();

    gl.setGlCorEntriesType(
        objectFactory.createXbrliXbrlGlCorAccountingEntriesGlCorDocumentInfoGlCorEntriesType());

    gl.getGlCorEntriesType().setContextRef(CONTEXT_REF);
    gl.getGlCorEntriesType().setDataValue(JOURNAL);

    gl.setGlCorUniqueID(
        objectFactory.createXbrliXbrlGlCorAccountingEntriesGlCorDocumentInfoGlCorUniqueID());
    gl.getGlCorUniqueID().setContextRef(CONTEXT_REF);
    gl.getGlCorUniqueID().setUniqueID("0001");

    gl.setGlCorLanguage(
        objectFactory.createXbrliXbrlGlCorAccountingEntriesGlCorDocumentInfoGlCorLanguage());
    gl.getGlCorLanguage().setContextRef(CONTEXT_REF);
    gl.getGlCorLanguage().setLanguage(DEFAULT_LANGUAGE);

    gl.setGlBusCreator(
        objectFactory.createXbrliXbrlGlCorAccountingEntriesGlCorDocumentInfoGlBusCreator());
    gl.getGlBusCreator().setContextRef(CONTEXT_REF);
    gl.getGlBusCreator().setCreator(CREATOR);

    gl.setGlCorCreationDate(
        objectFactory.createXbrliXbrlGlCorAccountingEntriesGlCorDocumentInfoGlCorCreationDate());
    gl.getGlCorCreationDate().setContextRef(CONTEXT_REF);
    gl.getGlCorCreationDate().setCreationDate(new Date());

    gl.setGlCorEntriesComment(
        objectFactory.createXbrliXbrlGlCorAccountingEntriesGlCorDocumentInfoGlCorEntriesComment());
    gl.getGlCorEntriesComment().setContextRef(CONTEXT_REF);
    gl.getGlCorEntriesComment().setEntriesComment(SOURCE_APP);

    //	gl-cor:periodCoveredStart - From the query

    gl.setGlCorPeriodCoveredStart(
        objectFactory
            .createXbrliXbrlGlCorAccountingEntriesGlCorDocumentInfoGlCorPeriodCoveredStart());
    gl.getGlCorPeriodCoveredStart().setContextRef(CONTEXT_REF);
    gl.getGlCorPeriodCoveredStart().setPeriodCoveredStart(new Date());

    gl.setGlCorPeriodCoveredEnd(
        objectFactory
            .createXbrliXbrlGlCorAccountingEntriesGlCorDocumentInfoGlCorPeriodCoveredEnd());
    gl.getGlCorPeriodCoveredEnd().setPeriodCoveredEnd(new Date());
    gl.getGlCorPeriodCoveredEnd().setContextRef(CONTEXT_REF);

    gl.setGlBusSourceApplication(
        objectFactory
            .createXbrliXbrlGlCorAccountingEntriesGlCorDocumentInfoGlBusSourceApplication());
    gl.getGlBusSourceApplication().setContextRef(CONTEXT_REF);
    gl.getGlBusSourceApplication().setSourceApplication(SOURCE_APP);

    gl.setGlMucDefaultCurrency(
        new XbrliXbrl.GlCorAccountingEntries.GlCorDocumentInfo.GlMucDefaultCurrency());
    gl.getGlMucDefaultCurrency().setContextRef(CONTEXT_REF);
    gl.getGlMucDefaultCurrency().setDefaultCurrency(DEFAULT_CURRENCY);

    return gl;
  }

  public XbrliXbrl.GlCorAccountingEntries.GlCorEntityInformation createGlCorEntityInformation(
      Optional<Company> companyOptional) {
    XbrliXbrl.GlCorAccountingEntries.GlCorEntityInformation entityInformation =
        objectFactory.createXbrliXbrlGlCorAccountingEntriesGlCorEntityInformation();

    if (companyOptional.isPresent()) {
      Company company = companyOptional.get();
      entityInformation.setGlBusOrganizationIdentifiers(
          objectFactory
              .createXbrliXbrlGlCorAccountingEntriesGlCorEntityInformationGlBusOrganizationIdentifiers());
      entityInformation
          .getGlBusOrganizationIdentifiers()
          .setGlBusOrganizationDescription(
              objectFactory
                  .createXbrliXbrlGlCorAccountingEntriesGlCorEntityInformationGlBusOrganizationIdentifiersGlBusOrganizationDescription());
      entityInformation
          .getGlBusOrganizationIdentifiers()
          .getGlBusOrganizationDescription()
          .setOrganizationDescription(company.getName());
        entityInformation
                .getGlBusOrganizationIdentifiers()
                .getGlBusOrganizationDescription()
                .setContextRef(CONTEXT_REF);

      entityInformation
          .getGlBusOrganizationIdentifiers()
          .setGlBusOrganizationIdentifier(
              objectFactory
                  .createXbrliXbrlGlCorAccountingEntriesGlCorEntityInformationGlBusOrganizationIdentifiersGlBusOrganizationIdentifier());

      entityInformation
          .getGlBusOrganizationIdentifiers()
          .getGlBusOrganizationIdentifier()
          .setOrganizationIdentifier(company.getRegistrationNumber());
        entityInformation
                .getGlBusOrganizationIdentifiers()
                .getGlBusOrganizationIdentifier()
                .setContextRef(CONTEXT_REF);
    }
    return entityInformation;
  }

  public XbrliXbrl createXbrl() {
    XbrliXbrl.GlCorAccountingEntries ae = objectFactory.createXbrliXbrlGlCorAccountingEntries();
    XbrliXbrl result = objectFactory.createXbrliXbrl();
    result.setGlCorAccountingEntries(ae);
    return result;
  }
}
