package com.balances.nsgpoc.factory;

import com.balances.nsgpoc.model.Transaction;
import com.balances.nsgpoc.model.gl.ObjectFactory;
import com.balances.nsgpoc.model.gl.XbrliXbrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.balances.nsgpoc.utils.Constants.DEFAULT_CONTEXT_REF;

@Component
public class EntryHeaderFactory {

  private static String ACCOUNTANT = "Demo Accountant";
  private static String SOURCE_JOURNAL_ID = "gj";
  private static String SOURCE_JOURNAL_DESCRIPTION = "General Ledger Journals";

  @Autowired private ObjectFactory objectFactory;

  public XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader createEntryHeader(
      Transaction transaction) {
    XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader result =
        objectFactory.createXbrliXbrlGlCorAccountingEntriesGlCorEntryHeader();

    result.setGlCorEnteredBy(
        objectFactory.createXbrliXbrlGlCorAccountingEntriesGlCorEntryHeaderGlCorEnteredBy());
    result.getGlCorEnteredBy().setEnteredBy(ACCOUNTANT);
    result.getGlCorEnteredBy().setContextRef(DEFAULT_CONTEXT_REF);

    result.setGlCorSourceJournalID(getSourceJournalID());
    result.getGlCorSourceJournalID().setSourceJournalID(SOURCE_JOURNAL_ID);
    result.getGlCorSourceJournalID().setContextRef(DEFAULT_CONTEXT_REF);

    result.setGlBusSourceJournalDescription(getSourceJournalDescription());
    result
        .getGlBusSourceJournalDescription()
        .setSourceJournalDescription(SOURCE_JOURNAL_DESCRIPTION);
    result
            .getGlBusSourceJournalDescription()
            .setContextRef(DEFAULT_CONTEXT_REF);

    result.setGlCorEntryNumber(
        objectFactory.createXbrliXbrlGlCorAccountingEntriesGlCorEntryHeaderGlCorEntryNumber());
    result.getGlCorEntryNumber().setEntryNumber(transaction.getTransactionID());
    result.getGlCorEntryNumber().setContextRef(DEFAULT_CONTEXT_REF);


    result.setGlCorEntryComment(
        objectFactory.createXbrliXbrlGlCorAccountingEntriesGlCorEntryHeaderGlCorEntryComment());
    result.getGlCorEntryComment().setEntryComment(transaction.getTransactionDescription());
    result.getGlCorEntryComment().setContextRef(DEFAULT_CONTEXT_REF);

    return result;
  }

  private XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlBusSourceJournalDescription
      getSourceJournalDescription() {
    XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlBusSourceJournalDescription sourceJournalDescription =
        new XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlBusSourceJournalDescription();
    return sourceJournalDescription;
  }

  private XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorSourceJournalID
      getSourceJournalID() {
    XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorSourceJournalID result =
        new XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorSourceJournalID();
    return result;
  }

  //gl-cor:entryDetail
  public XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail createEntryDetail() {
    XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail result =
        objectFactory.createXbrliXbrlGlCorAccountingEntriesGlCorEntryHeaderGlCorEntryDetail();

    result.setGlCorLineNumber(getGlCorLineNumber());

    result.setGlCorAmount(getGlCorAmount());

    result.setGlCorAmount(getAmount());
    result.setGlCorDebitCreditCode(getDebitCreditCode());

    result.setGlCorPostingDate(getPostingDate());
    result.setGlCorDocumentType(getDocumentType());
    result.setGlCorPostingStatus(getPostingStatus());

    return result;
  }

  private XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorPostingStatus
      getPostingStatus() {
    XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorPostingStatus result =
        new XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorPostingStatus();
    return result;
  }

  private XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorDocumentType
      getDocumentType() {
    XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorDocumentType result =
        this.objectFactory
            .createXbrliXbrlGlCorAccountingEntriesGlCorEntryHeaderGlCorEntryDetailGlCorDocumentType();
    return result;
  }

  private XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorPostingDate
      getPostingDate() {
    XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorPostingDate result =
        this.objectFactory
            .createXbrliXbrlGlCorAccountingEntriesGlCorEntryHeaderGlCorEntryDetailGlCorPostingDate();
    return result;
  }

  private XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorDebitCreditCode
      getDebitCreditCode() {
    XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorDebitCreditCode result =
        this.objectFactory
            .createXbrliXbrlGlCorAccountingEntriesGlCorEntryHeaderGlCorEntryDetailGlCorDebitCreditCode();
    return result;
  }

  private XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorAmount
      getAmount() {
    XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorAmount result =
        this.objectFactory
            .createXbrliXbrlGlCorAccountingEntriesGlCorEntryHeaderGlCorEntryDetailGlCorAmount();

    return result;
  }

  private XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorAmount
      getGlCorAmount() {
    return this.objectFactory
        .createXbrliXbrlGlCorAccountingEntriesGlCorEntryHeaderGlCorEntryDetailGlCorAmount();
  }

  private XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorLineNumber
      getGlCorLineNumber() {
    return this.objectFactory
        .createXbrliXbrlGlCorAccountingEntriesGlCorEntryHeaderGlCorEntryDetailGlCorLineNumber();
  }

  public XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorAccount
      createEmptyAccount() {
    XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorAccount account =
        objectFactory
            .createXbrliXbrlGlCorAccountingEntriesGlCorEntryHeaderGlCorEntryDetailGlCorAccount();

    account.setGlCorAccountMainID(getAccountMainID(objectFactory));
    account.setGlCorAccountMainDescription(getAccountMainDescription(objectFactory));
    account.setGlCorAccountPurposeCode(getAccountPurposeCode(objectFactory));

    account.setGlCorAccountType(getAccountType(objectFactory));

    return account;
  }

  private XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorAccount
          .GlCorAccountMainID
      getAccountMainID(ObjectFactory of) {
    XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorAccount
            .GlCorAccountMainID
        result =
            of
                .createXbrliXbrlGlCorAccountingEntriesGlCorEntryHeaderGlCorEntryDetailGlCorAccountGlCorAccountMainID();
    return result;
  }

  private XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorAccount
          .GlCorAccountMainDescription
      getAccountMainDescription(ObjectFactory of) {
    XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorAccount
            .GlCorAccountMainDescription
        result =
            of
                .createXbrliXbrlGlCorAccountingEntriesGlCorEntryHeaderGlCorEntryDetailGlCorAccountGlCorAccountMainDescription();
    return result;
  }

  private XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorAccount
          .GlCorAccountPurposeCode
      getAccountPurposeCode(ObjectFactory of) {
    XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorAccount
            .GlCorAccountPurposeCode
        result =
            new XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorAccount
                .GlCorAccountPurposeCode();
    return result;
  }

  private XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorAccount
          .GlCorAccountType
      getAccountType(ObjectFactory of) {
    XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorAccount.GlCorAccountType
        result =
            new XbrliXbrl.GlCorAccountingEntries.GlCorEntryHeader.GlCorEntryDetail.GlCorAccount
                .GlCorAccountType();
    return result;
  }
}
