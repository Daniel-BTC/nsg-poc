package com.balances.nsgpoc.model.gl;

import lombok.SneakyThrows;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateAdapter extends XmlAdapter<String, Date> {

    private static final ThreadLocal<DateFormat> dateFormat
            = new ThreadLocal<DateFormat>() {

        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd");
        }
    };

    @SneakyThrows
    @Override
    public Date unmarshal(String v) {
        return dateFormat.get().parse(v);
    }

    @Override
    public String marshal(Date v) {
        return dateFormat.get().format(v);
    }
}
