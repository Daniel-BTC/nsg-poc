package com.balances.nsgpoc.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "COMPANY")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
    name = "CompanyStructure",
    propOrder = {
      "registrationNumber",
      "name",
      "address",
      "contact",
      "taxRegistration",
      "bankAccount"
    })
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Company implements Serializable {
  private static final long serialVersionUID = 2255035775913671894L;

  @Column(name = "company_sid")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  @XmlTransient
  private Long companySID;

  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "uuid", nullable = false, unique = true)
  @XmlTransient
  private UUID uuid;

  @Column(name = "registration_numbers", nullable = false, unique = true)
  @XmlElement(name = "RegistrationNumber")
  private String registrationNumber;

  @Column(name = "name", nullable = false)
  @XmlElement(name = "Name", required = true)
  private String name;

  @Column(name = "record_status")
  @XmlTransient
  private Boolean recordStatus;

  @Column(name = "createdDate", nullable = false)
  @XmlTransient
  private Date createdDate;

  @Column(name = "createdBy")
  @XmlTransient
  private String createdBy;

  @Column(name = "modifiedDate")
  @XmlTransient
  private Date modifiedDate;

  @Column(name = "modifiedBy")
  @XmlTransient
  private String modifiedBy;

  @Column(name = "modified_count", nullable = false)
  @XmlTransient
  private Integer modifiedCount;

  @Column(name = "is_default", nullable = false)
  @XmlTransient
  private Boolean isDefaultCompany;

  @Column(name = "account_system")
  @XmlTransient
  private String accountSystem;

  @Column(name = "account_system_username")
  @XmlTransient
  private String accountSystemUsername;

  @Column(name = "account_system_password")
  @XmlTransient
  private String accountSystemPassword;

  @Column(name = "account_system_ientitycode", unique = true)
  @XmlTransient
  private String accountSystemIEntityCode;

  @Column(name = "country_code")
  @XmlTransient
  private String countryCode;

  @Column(name = "period_type", nullable = false)
  @XmlTransient
  private String periodType;

  @Column(name = "period_no")
  @XmlTransient
  private Integer periodNo;

  @Column(name = "lock_period_end")
  @XmlTransient
  private Integer lockPeriodEnd;

  @Column(name = "lock_period_start")
  @XmlTransient
  private Integer lockPeriodStart;

  @Column(name = "subscription_valid_date")
  @XmlTransient
  private Date subscriptionValidDate;

  @Column(name = "current_working_period_end")
  @XmlTransient
  private Integer currentWorkingPeriodEnd;

  @Column(name = "current_working_period_start")
  @XmlTransient
  private Integer currentWorkingPeriodStart;

  @Column(name = "current_accounting_year")
  @XmlTransient
  private Integer currentAccountingYear;

//  @Column(name = "tripletex_employee_token")
//  @XmlTransient
//  private String tripletexEmployeeToken;

//  @Column(name = "tripletex_company_id")
//  @XmlTransient
//  private String tripletexCompanyId;

  @Column(name = " arbitrary_amount")
  @XmlTransient
  private Integer arbitraryAmount;

  @Column(name = "user_sid_account_holder")
  @XmlTransient
  private Long accountHolder;

  @Column(name = "user_sid_approver")
  @XmlTransient
  private Long approver;

  @Column(name = "altinn_username")
  @XmlTransient
  private String altinnUsername;

  @Column(name = "altinn_password")
  @XmlTransient
  private String altinnPassword;

  @Column(name = "altinn_userssn")
  @XmlTransient
  private String altinnUserSSN;

  @Column(name = "altinn_reportee")
  @XmlTransient
  private String altinnReporteeId;

  @Column(name = "twenty_four_seven_id")
  @XmlTransient
  private String twentyFourSevenId;

  @Column(name = "twenty_four_seven_company_name")
  @XmlTransient
  private String twentyFourSevenCompanyName;

//  @Column(name = "tripletex_company_name")
//  @XmlTransient
//  private String tripletexCompanyName;

  @Column(name = "cma_app_key")
  @XmlTransient
  private String connectMyAppsAppKey;

  @Column(name = "cma_session_key")
  @XmlTransient
  private String connectMyAppsSessionKey;

  @Column(name = "cma_client_id")
  @XmlTransient
  private String connectMyAppsClientId;

  @Column(name = "is_main_company")
  @XmlTransient
  private Boolean isMainCompany = false;

  @Column(name = "has_organization_altinn_settings")
  @XmlTransient
  private Boolean hasOrganizationAltinnSettings = false;

  @Column(name = "altinn_update_status")
  @XmlTransient
  private Boolean altinnUpdateStatus;

  @Column(name = "visma_database_code")
  @XmlTransient
  private String vismaDatabaseCode;
}
