package com.balances.nsgpoc.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "AMOUNT")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "AmountStructure",
        propOrder = {"amount", "currencyCode", "currencyAmount", "exchangeRate"})
public class Amount implements Serializable {
    private static final long serialVersionUID = 1463320341198883950L;

    @Column(name = "amount_sid", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @XmlTransient
    private Long amountSID;

    @Column(name = "amount", scale = 2, precision = 20)
    @XmlElement(name = "Amount", required = true)
    private BigDecimal amount;

    @Column(name = "currency_codes", length = 18)
    @XmlElement(name = "CurrencyCode")
    private String currencyCode;

    @Column(name = "currency_amounts", scale = 2, precision = 20)
    @XmlElement(name = "CurrencyAmount")
    private BigDecimal currencyAmount;

    @Column(name = "exchange_rates", scale = 8, precision = 26)
    @XmlElement(name = "ExchangeRate")
    private BigDecimal exchangeRate;

    @JsonIgnore
    @OneToOne(mappedBy = "creditAmount")
    @XmlTransient
    private Line creditLine;

    @JsonIgnore
    @OneToOne(mappedBy = "debitAmount")
    @XmlTransient
    private Line debitLine;

    @Column(name = "account_type", length = 18)
    @XmlTransient
    private String accountType;

    @Column(name = "type", length = 18)
    @XmlTransient
    private String type;
}
