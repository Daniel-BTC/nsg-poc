package com.balances.nsgpoc.model;

import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Data
public class CompanyTransactionDocument {

    private Optional<Company> companyOptional;
    private Date start;
    private Date end;
    private List<Transaction> transactions;
}
