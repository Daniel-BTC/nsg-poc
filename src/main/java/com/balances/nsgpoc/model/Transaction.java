package com.balances.nsgpoc.model;

import lombok.Data;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "TRANSACTION")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
    name = "",
    propOrder = {
      "transactionID",
      "period",
      "periodYear",
      "transactionDate",
      "sourceID",
      "transactionType",
      "transactionDescription",
      "batchID",
      "systemEntryDate",
      "glPostingDate",
      "customerID",
      "supplierID",
      "systemID",
      "line"
    })
public class Transaction implements Serializable {
  private static final long serialVersionUID = -3688220840151943634L;

  @Column(name = "transaction_sid")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  @XmlTransient
  private Long transactionSID;

  @Column(name = "id")
  @XmlTransient
  private String id;

  @Column(name = "transaction_id", length = 200, nullable = false)
  @XmlElement(name = "TransactionID", required = true)
  private String transactionID;

  @Column(name = "period")
  @XmlElement(name = "Period", required = true)
  @XmlSchemaType(name = "nonNegativeInteger")
  private Integer period;

  @Column(name = "period_year")
  @XmlElement(name = "PeriodYear")
  private Integer periodYear;

  @Column(name = "transaction_date")
  @XmlElement(name = "TransactionDate", required = true)
  @XmlSchemaType(name = "date")
  private Date transactionDate;

  @Column(name = "source_id", length = 255)
  @XmlElement(name = "SourceID")
  private String sourceID;

  @Column(name = "transaction_type", length = 255)
  @XmlElement(name = "TransactionType")
  private String transactionType;

  @Column(name = "transaction_description", length = 255)
  @XmlElement(name = "Description", required = true)
  private String transactionDescription;

  @Column(name = "batch_id", length = 255)
  @XmlElement(name = "BatchID")
  private String batchID;

  @Column(name = "system_entry_date")
  @XmlElement(name = "SystemEntryDate", required = true)
  @XmlSchemaType(name = "date")
  private Date systemEntryDate;

  @Column(name = "gl_posting_dates")
  @XmlElement(name = "GLPostingDate", required = true)
  @XmlSchemaType(name = "date")
  private Date glPostingDate;

  @XmlElement(name = "CustomerID")
  private String customerID;

  @XmlElement(name = "SupplierID")
  private String supplierID;

  @Column(name = "system_id", length = 18)
  @XmlElement(name = "SystemID")
  private String systemID;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "transaction")
  @XmlElement(name = "Line", required = true)
  private List<Line> line;

  @JoinColumn(name = "journal_sid", nullable = false)
  @ManyToOne(fetch = FetchType.LAZY)
  @XmlTransient
  private Journal journal;

  @Column(name = "gltransaction_id", length = 70)
  @XmlTransient
  private Long gltransactionId;

  @Column(name = "record_status", nullable = false)
  @XmlTransient
  private Boolean recordStatus;

  @Column(name = "createdDate", nullable = false)
  @XmlTransient
  private Date createdDate;

  @Column(name = "createdBy", length = 70)
  @XmlTransient
  private String createdBy;

  @Column(name = "modifiedDate")
  @XmlTransient
  private Date modifiedDate;

  @Column(name = "modifiedBy", length = 70)
  @XmlTransient
  private String modifiedBy;

  @Column(name = "modified_count", nullable = false)
  @XmlTransient
  private Integer modifiedCount;

  @JoinColumn(name = "company_sid")
  @ManyToOne(fetch = FetchType.LAZY)
  @XmlTransient
  private Company company;

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((company == null) ? 0 : company.hashCode());
    result = prime * result + ((period == null) ? 0 : period.hashCode());
    result = prime * result + ((periodYear == null) ? 0 : periodYear.hashCode());

    result = prime * result + ((transactionID == null) ? 0 : transactionID.hashCode());

    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    Transaction other = (Transaction) obj;
    if (company == null) {
      if (other.company != null) return false;
    } else if (!company.equals(other.company)) {
      return false;
    }
    if (period == null) {
      if (other.period != null) return false;
    } else if (!period.equals(other.period)) {
      return false;
    }
    if (periodYear == null) {
      if (other.periodYear != null) return false;
    } else if (!periodYear.equals(other.periodYear)) {
      return false;
    }
    if (transactionID == null) {
      return other.transactionID == null;
    } else {
      return transactionID.equals(other.transactionID);
    }
  }
}
