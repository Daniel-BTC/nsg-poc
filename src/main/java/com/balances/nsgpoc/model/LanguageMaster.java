package com.balances.nsgpoc.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "language_master")
public class LanguageMaster implements Serializable {
  private static final long serialVersionUID = -8944379478857278954L;

  @Column(name = "language_sid")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  private Long languageSID;

  @Column(name = "language")
  private String language;

  @Column(name = "language_code", unique = true)
  private String languageCode;
}
