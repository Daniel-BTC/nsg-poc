package com.balances.nsgpoc.manager;

import com.balances.nsgpoc.dto.PeriodCoveredDTO;
import com.balances.nsgpoc.model.Transaction;
import com.balances.nsgpoc.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class TransactionNSGManager {

  @Autowired private TransactionRepository transactionRepository;

  @Transactional(readOnly = true)
  public List<Transaction> findTransactionsForDefaultCustomer(
      Long companyId, Date startDay, Date endDay) {

    return transactionRepository.findTransactionsByCustomerId(
            companyId,
            startDay,
            endDay);
  }

  @Transactional(readOnly = true)
  public PeriodCoveredDTO getMinMaxDateByCompany(Long companyId) {

    List<Date> dates = transactionRepository.findMinDateTransactionsByCustomerId(companyId);

    Optional<Date> max = dates.stream().max(Date::compareTo);
    Optional<Date> min = dates.stream().min(Date::compareTo);

    PeriodCoveredDTO result = new PeriodCoveredDTO(min, max);
    return result;
  }

  @Transactional(readOnly = true)
  public List<Transaction> findTransactionsByCustomerIdAndTransactionID(Long companyId, String id) {
    return transactionRepository.findTransactionsByCustomerIdAndTransactionID(companyId, id);
  }
}
