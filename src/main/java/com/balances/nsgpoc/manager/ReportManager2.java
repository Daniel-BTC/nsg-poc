package com.balances.nsgpoc.manager;

import com.balances.nsgpoc.dto.PeriodCoveredDTO;
import com.balances.nsgpoc.factory.EntryHeaderFactory;
import com.balances.nsgpoc.model.Company;
import com.balances.nsgpoc.model.CompanyTransactionDocument;
import com.balances.nsgpoc.model.Line;
import com.balances.nsgpoc.model.Transaction;
import com.balances.nsgpoc.model.gl.DateAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xbrl._2003.instance.Unit;
import org.xbrl._2003.instance.Xbrl;
import org.xbrl._2003.xlink.SimpleType;
import org.xbrl._int.gl.bus._2016_12_01.*;
import org.xbrl._int.gl.cor._2016_12_01.*;
import org.xbrl._int.gl.gen._2016_12_01.*;
import org.xbrl._int.gl.muc._2016_12_01.CurrencyItemType;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import static com.balances.nsgpoc.utils.Constants.DEFAULT_CONTEXT_REF;

@Service
public class ReportManager2 {

  private static String XLINK = "../plt/case-c-b-m/gl-plt-all-2016-12-01.xsd";
  private static String ARCROLE = "http://www.w3.org/1999/xlink/properties/linkbase";
  private static String XLINK_TYPE = "simple";

  private static String ACCOUNT_TYPE = "account";
  private static String DECIMALS_AFTER_POINT = "2";
  private static String DEBIT_CODE = "D";
  private static String CREDIT_CODE = "C";
  private static String CURRENCY_NOK = "nok";
  private static String POSTING_STATUS = "posted";
  private static String VOUCHER = "voucher";

  private static String CONTEXT_REF = "now";
  private static String SOURCE_APP = "NSG PoC";
  private static String DEFAULT_CURRENCY = "iso4217:nok";
  private static String DEFAULT_LANGUAGE = "iso639:nb";
  private static String JOURNAL = "journal";
  private static String CREATOR = "Accountflow";

  private static String ACCOUNTANT = "Demo Accountant";
  private static String SOURCE_JOURNAL_ID = "gj";
  private static String SOURCE_JOURNAL_DESCRIPTION = "General Ledger Journals";

  @Autowired private DocumentManager documentManager;
  @Autowired private EntryHeaderFactory entryHeaderFactory;
  @Autowired private TransactionNSGManager transactionManager;;

  public Xbrl getReportForCompany(Long companyId, Optional<Date> startDay, Optional<Date> endDay) {

    CompanyTransactionDocument dto =
        documentManager.createDocumentForCompany(companyId, startDay, endDay);

    Xbrl xbrl2 = new Xbrl();
    fillLinkSchema(xbrl2);

    // objectFactory.createTuple();
    org.xbrl._int.gl.cor._2016_12_01.ObjectFactory objectFactory2 =
        new org.xbrl._int.gl.cor._2016_12_01.ObjectFactory();

    AccountingEntriesComplexType accountingEntries =
        objectFactory2.createAccountingEntriesComplexType();

    JAXBElement<AccountingEntriesComplexType> accountingEntriesComplexTypeJAXBElement =
        objectFactory2.createAccountingEntries(accountingEntries);

    xbrl2.getItemOrTupleOrContext().add(accountingEntriesComplexTypeJAXBElement);

    PeriodCoveredDTO dates = transactionManager.getMinMaxDateByCompany(companyId);

    if (startDay.isPresent()) {
      dates.setMinOptional(startDay);
    }
    if (endDay.isPresent()) {
      dates.setMaxOptional(endDay);
    }

    accountingEntries.setDocumentInfo(this.createDocumentInfo());
    setPeriodCovered(accountingEntries, dates);

    accountingEntries.setEntityInformation(this.createEntityInformation(dto.getCompanyOptional()));

    xbrl2.getItemOrTupleOrContext().add(accountingEntries);
    if (Objects.nonNull(dto.getTransactions())) {
      List<Transaction> transactions = dto.getTransactions();
      transactions.forEach(
          t -> {
            EntryHeaderComplexType entryHeader = createEntryHeader(t);
            accountingEntries.getEntryHeader().add(entryHeader);

            List<Line> lines = t.getLine();

            AtomicInteger counter = new AtomicInteger(1);
            for (Line line : lines) {

              EntryDetailComplexType entryDetail = objectFactory2.createEntryDetailComplexType();
              entryDetail.setLineNumber(objectFactory2.createLineNumberItemType());
              entryDetail.getLineNumber().setValue(Integer.toString(counter.get()));
              entryDetail.getLineNumber().setContextRef(DEFAULT_CONTEXT_REF);


              AccountComplexType account = objectFactory2.createAccountComplexType();

              // updateAccount(account, line);
              account.setAccountMainID(objectFactory2.createAccountMainIDItemType());
              account.getAccountMainID().setValue(line.getAccountID());
              account.getAccountMainID().setContextRef(DEFAULT_CONTEXT_REF);

              account.setAccountMainDescription(
                  objectFactory2.createAccountMainDescriptionItemType());
              account.getAccountMainDescription().setValue("Value");
              account.getAccountMainDescription().setContextRef(DEFAULT_CONTEXT_REF);

              account.setAccountPurposeCode(new AccountPurposeCodeItemType());
              account.getAccountPurposeCode().setValue("usgaap");
              account.getAccountPurposeCode().setContextRef(DEFAULT_CONTEXT_REF);

              account.setAccountType(new AccountTypeItemType());
              account.getAccountType().setValue(ACCOUNT_TYPE);
              account.getAccountType().setContextRef(DEFAULT_CONTEXT_REF);

              entryDetail.getAccount().add(account);

              // updateEntryDetail(entryDetail, line);

              entryDetail.setAmount(new AmountItemType());
              entryDetail.setDebitCreditCode(new DebitCreditCodeItemType());
              entryDetail.getAmount().setDecimals(DECIMALS_AFTER_POINT);
              entryDetail.getAmount().setValue(BigDecimal.ZERO);
              entryDetail.getAmount().setContextRef(DEFAULT_CONTEXT_REF);
              if (Objects.nonNull(line.getCreditAmount())
                  && Objects.nonNull(line.getCreditAmount().getAmount())) {
                BigDecimal amount = line.getCreditAmount().getAmount();
                entryDetail.getAmount().setValue(amount);
                entryDetail.getAmount().setContextRef(DEFAULT_CONTEXT_REF);
                entryDetail.getDebitCreditCode().setValue(CREDIT_CODE);
                entryDetail.getDebitCreditCode().setContextRef(DEFAULT_CONTEXT_REF);
              } else if (Objects.nonNull(line.getDebitAmount())
                  && Objects.nonNull(line.getDebitAmount().getAmount())) {
                BigDecimal amount = line.getDebitAmount().getAmount();
                entryDetail.getAmount().setValue(amount);
                entryDetail.getAmount().setContextRef(DEFAULT_CONTEXT_REF);
                entryDetail.getDebitCreditCode().setValue(DEBIT_CODE);
                entryDetail.getDebitCreditCode().setContextRef(DEFAULT_CONTEXT_REF);
              }

              entryHeader.getEntryDetail().add(entryDetail);

              Unit unitNok = new Unit();
              unitNok.setId(DEFAULT_CURRENCY);
              unitNok.getMeasure().add(new QName(DEFAULT_CURRENCY));
              entryDetail.getAmount().setUnitRef(unitNok);

              entryDetail.getAmount().setContextRef(DEFAULT_CONTEXT_REF);

              entryDetail.setPostingDate(new PostingDateItemType());
              entryDetail
                  .getPostingDate()
                  .setValue(new DateAdapter().marshal(line.getSystemEntryTime()));
              entryDetail.getPostingDate().setContextRef(DEFAULT_CONTEXT_REF);

              entryDetail.setPostingStatus(new PostingStatusItemType());
              entryDetail.getPostingStatus().setValue(POSTING_STATUS);
              entryDetail.getPostingStatus().setContextRef(DEFAULT_CONTEXT_REF);

              entryDetail.setDocumentType(new DocumentTypeItemType());
              entryDetail.getDocumentType().setValue(VOUCHER);
              entryDetail.getDocumentType().setContextRef(DEFAULT_CONTEXT_REF);

              counter.incrementAndGet();
            }
          });
    }
    return xbrl2;
  }

  public EntryHeaderComplexType createEntryHeader(Transaction transaction) {
    EntryHeaderComplexType result = new EntryHeaderComplexType();

    result.setEnteredBy(new EnteredByItemType());
    result.getEnteredBy().setValue(ACCOUNTANT);
    result.getEnteredBy().setContextRef(DEFAULT_CONTEXT_REF);

    return result;
  }

  private EntityInformationComplexType createEntityInformation(Optional<Company> companyOptional) {
    EntityInformationComplexType entityInformation = new EntityInformationComplexType();

    if (companyOptional.isPresent()) {
      Company company = companyOptional.get();

      OrganizationIdentifiersComplexType organizationIdentifier =
          new OrganizationIdentifiersComplexType();
      OrganizationDescriptionItemType organizationDescription =
          new OrganizationDescriptionItemType();
      organizationDescription.setValue(company.getName());
      organizationDescription.setContextRef(CONTEXT_REF);

      OrganizationIdentifierItemType organizationIdentifierItem =
          new OrganizationIdentifierItemType();
      organizationIdentifierItem.setValue(company.getRegistrationNumber());
      organizationIdentifierItem.setContextRef(CONTEXT_REF);

      organizationIdentifier.setOrganizationDescription(organizationDescription);
      organizationIdentifier.setOrganizationIdentifier(organizationIdentifierItem);
      entityInformation.getOrganizationIdentifiers().add(organizationIdentifier);
    }

    return entityInformation;
  }

  private void setPeriodCovered(
      AccountingEntriesComplexType accountingEntries, PeriodCoveredDTO dates) {

    accountingEntries
        .getDocumentInfo()
        .getPeriodCoveredStart()
        .setValue(new DateAdapter().marshal(dates.getMinOptional().orElse(new Date())));

    accountingEntries
        .getDocumentInfo()
        .getPeriodCoveredEnd()
        .setValue(new DateAdapter().marshal(dates.getMaxOptional().orElse(new Date())));
    ;
  }

  private void fillLinkSchema(Xbrl xbrl) {
    SimpleType type = new SimpleType();
    type.setType(XLINK_TYPE);
    type.setArcrole(ARCROLE);
    type.setHref(XLINK);
    xbrl.getSchemaRef().add(type);
  }

  private DocumentInfoComplexType createDocumentInfo() {

    var gl = new DocumentInfoComplexType();

    EntriesTypeItemType type = new EntriesTypeItemType();
    type.setContextRef(CONTEXT_REF);
    type.setValue(JOURNAL);
    gl.setEntriesType(type);

    UniqueIDItemType uniqueID = new UniqueIDItemType();
    gl.setUniqueID(uniqueID);
    uniqueID.setContextRef(CONTEXT_REF);
    // Generated by app
    uniqueID.setValue("0001");

    Language language = new Language();
    gl.setLanguage(language);
    language.setContextRef(CONTEXT_REF);
    language.setValue(DEFAULT_LANGUAGE);

    CreatorItemType creator = new CreatorItemType();
    gl.setCreator(creator);
    creator.setContextRef(CONTEXT_REF);
    creator.setValue(CREATOR);

    CreationDateItemType creationDate = new CreationDateItemType();
    gl.setCreationDate(creationDate);
    creationDate.setContextRef(CONTEXT_REF);
    creationDate.setValue(new DateAdapter().marshal(new Date()));

    EntriesCommentItemType entriesComment = new EntriesCommentItemType();
    gl.setEntriesComment(entriesComment);
    entriesComment.setContextRef(CONTEXT_REF);
    entriesComment.setValue(SOURCE_APP);

    PeriodCoveredStartItemType periodCoveredStart = new PeriodCoveredStartItemType();
    gl.setPeriodCoveredStart(periodCoveredStart);
    periodCoveredStart.setContextRef(CONTEXT_REF);
    periodCoveredStart.setValue(new DateAdapter().marshal(new Date()));

    PeriodCoveredEndItemType periodCoveredEnd = new PeriodCoveredEndItemType();
    gl.setPeriodCoveredEnd(periodCoveredEnd);
    periodCoveredEnd.setContextRef(CONTEXT_REF);
    periodCoveredEnd.setValue(new DateAdapter().marshal(new Date()));

    // unit need change to EU
    Unit unit = new Unit();
    unit.setId("NotUsed");
    unit.getMeasure().add(new QName("pure"));

    PeriodCountItemType periodCountItemType = new PeriodCountItemType();
    gl.setPeriodCount(periodCountItemType);
    periodCountItemType.setContextRef(CONTEXT_REF);
    periodCountItemType.setValue(new BigDecimal(100));
    periodCountItemType.setUnitRef(unit);

    SourceApplicationItemType sourceApplicationItem = new SourceApplicationItemType();
    gl.setSourceApplication(sourceApplicationItem);
    sourceApplicationItem.setContextRef(CONTEXT_REF);
    sourceApplicationItem.setValue(SOURCE_APP);

    CurrencyItemType currencyItem = new CurrencyItemType();
    gl.setDefaultCurrency(currencyItem);
    currencyItem.setContextRef(CONTEXT_REF);
    QName qName = new QName(DEFAULT_CURRENCY);
    currencyItem.setValue(qName);

    PeriodUnitTypeItemType periodUnit = new PeriodUnitTypeItemType();
    gl.setPeriodUnit(periodUnit);
    periodUnit.setContextRef(CONTEXT_REF);
    periodUnit.setValue("other");

    PeriodUnitDescriptionItemType periodUnitDescription = new PeriodUnitDescriptionItemType();
    gl.setPeriodUnitDescription(periodUnitDescription);
    periodUnitDescription.setContextRef(CONTEXT_REF);
    periodUnitDescription.setValue("description");

    return gl;
  }
}
