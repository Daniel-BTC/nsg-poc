package com.balances.nsgpoc.manager;

import com.balances.nsgpoc.model.Company;
import com.balances.nsgpoc.model.CompanyTransactionDocument;
import com.balances.nsgpoc.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class DocumentManager {

    @Autowired
    private TransactionNSGManager transactionNSGManager;
    @Autowired
    private CompanyManager companyManager;

    public CompanyTransactionDocument createDocumentForCompany(
            Long companyId, Optional<Date> startDay, Optional<Date> endDay) {
        Optional<Company> companyOptional = companyManager.getCompany(companyId);
        Date start = startDay.orElse(new Date(0));
        Date end = endDay.orElse(new Date());
        List<Transaction> transactions =
                transactionNSGManager.findTransactionsForDefaultCustomer(companyId, start, end);

        CompanyTransactionDocument result = new CompanyTransactionDocument();
        result.setCompanyOptional(companyOptional);
        result.setTransactions(transactions);
        result.setStart(start);
        result.setEnd(end);
        return  result;
    }
}
