package com.balances.nsgpoc.repository;

import com.balances.nsgpoc.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

  @Query(
      value =
          "select c from Transaction as c where c.company.companySID =:companyID "
              + "and c.transactionDate >= :startDay and c.transactionDate <= :endDay ")
  List<Transaction> findTransactionsByCustomerId(
      @Param("companyID") Long companyID,
      @Param("startDay") Date startDay,
      @Param("endDay") Date endDay);

  @Query(
      value =
          "select c from Transaction as c where c.company.companySID =:companyID "
              + "and c.transactionID = :transactionID")
  List<Transaction> findTransactionsByCustomerIdAndTransactionID(
      @Param("companyID") Long companyID, @Param("transactionID") String transactionID);

  @Query(
      value =
          "select c.transactionDate from Transaction as c where c.company.companySID =:companyID")
  List<Date> findMinDateTransactionsByCustomerId(@Param("companyID") Long companyID);
}
