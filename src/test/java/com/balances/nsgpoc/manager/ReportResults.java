package com.balances.nsgpoc.manager;

public class ReportResults {

  public static final String EMPTY_RESULT =
      "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
          + "<xbrli:xbrl xmlns:xbrll=\"http://www.xbrl.org/2003/linkbase\" xmlns:iso639=\"http://www.xbrl.org/2005/iso639\" xmlns:gl-bus=\"http://www.xbrl.org/int/gl/bus/2016-12-01\" xmlns:gl-cor=\"http://www.xbrl.org/int/gl/cor/2016-12-01\" xmlns:gl-muc=\"http://www.xbrl.org/int/gl/muc/2016-12-01\" xmlns:iso4217=\"http://www.xbrl.org/2005/iso4217\" xmlns:xbrli=\"http://www.xbrl.org/2003/instance\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n"
          + "    <xbrll:schemaRef xlink:href=\"../plt/case-c-b-m/gl-plt-all-2016-12-01.xsd\" xlink:arcrole=\"http://www.w3.org/1999/xlink/properties/linkbase\" xlink:type=\"simple\"/>\n"
          + "    <xbrli:unit id=\"NotUsed\">\n"
          + "        <xbrli:measure>pure</xbrli:measure>\n"
          + "    </xbrli:unit>\n"
          + "    <gl-cor:accountingEntries>\n"
          + "        <gl-cor:documentInfo>\n"
          + "            <gl-cor:entriesType contextRef=\"now\">journal</gl-cor:entriesType>\n"
          + "            <gl-cor:uniqueID contextRef=\"now\">0001</gl-cor:uniqueID>\n"
          + "            <gl-cor:language contextRef=\"now\">iso639:nb</gl-cor:language>\n"
          + "            <gl-cor:creationDate contextRef=\"now\">2020-03-16</gl-cor:creationDate>\n"
          + "            <gl-bus:creator contextRef=\"now\">Accountflow</gl-bus:creator>\n"
          + "            <gl-cor:entriesComment contextRef=\"now\">NSG PoC</gl-cor:entriesComment>\n"
          + "            <gl-cor:periodCoveredStart contextRef=\"now\">2020-03-16</gl-cor:periodCoveredStart>\n"
          + "            <gl-cor:periodCoveredEnd contextRef=\"now\">2020-03-16</gl-cor:periodCoveredEnd>\n"
          + "            <gl-bus:sourceApplication contextRef=\"now\">NSG PoC</gl-bus:sourceApplication>\n"
          + "            <gl-muc:defaultCurrency contextRef=\"now\">iso4217:nok</gl-muc:defaultCurrency>\n"
          + "        </gl-cor:documentInfo>\n"
          + "    </gl-cor:accountingEntries>\n"
          + "</xbrli:xbrl>\n";
}
